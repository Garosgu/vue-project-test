import Home from './components/Home.vue'
import Register from './components/Register.vue'

export const routes = [
  { path: '', component: Home },
  { path: '/register', component: Register }
]
